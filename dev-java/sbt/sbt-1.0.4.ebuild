# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

# repoman gives LIVEVCS.unmasked even with EGIT_COMMIT, so create snapshot
inherit eutils java-pkg-2 # git-r3

L_PN="sbt-launch"
L_P="${L_PN}-${PV}"

SV="2.12"

DESCRIPTION="sbt is a build tool for Scala and Java projects that aims to do the basics well"
HOMEPAGE="http://www.scala-sbt.org/"
EGIT_COMMIT="v${PV}"
EGIT_REPO_URI="https://github.com/sbt/sbt.git"
SRC_URI="
	binary? (
	  https://github.com/sbt/sbt/releases/download/${EGIT_COMMIT}/${P}.tgz
	)"
LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="binary"
REQUIRED_USE="binary"

DEPEND=">=virtual/jdk-1.8"
RDEPEND=">=virtual/jre-1.8"

# test hangs or fails
RESTRICT="test"

JAVA_GENTOO_CLASSPATH="scala-${SV}"

# Note: to bump sbt, some things to try are:
# 1. Create the sbt src snapshot:
# git clone https://github.com/sbt/sbt.git ${P}
# cd ${P}
# git checkout v${PV}
# cd ..
# XZ_OPT=-9 tar --owner=portage --group=portage \
# -cJf /usr/portage/distfiles/${P}-src.tar.xz ${P}
# 2. remove the https://dev.gentoo.org/~gienah/snapshots/${P}-ivy2-deps.tar.xz
# https://dev.gentoo.org/~gienah/snapshots/${P}-sbt-deps.tar.xz and
# binary? ( https://dev.gentoo.org/~gienah/files/dist/${P}-gentoo-binary.tar.xz )
# from SRC_URI
# 3. Comment the sbt publishLocal line in src_compile.
# 4. try:
# FEATURES='noclean -test' emerge -v -1 dev-java/sbt
# It should fail in src_install since the sbt publishLocal is not done.
# Check if it downloads more stuff in
# src_compile to ${WORKDIR}/.ivy2 and ${WORKDIR}/.sbt.
# 5. If some of the downloads fail, it might be necessary to run the sbt compile
# again manually to obtain all the dependencies, if so:
# cd to ${S}
# export EROOT=/
# export WORKDIR='/var/tmp/portage/dev-java/${P}/work'
# export SV="2.11"
# export L_P=${P}
# export PATH="/usr/share/scala-${SV}/bin:${WORKDIR}/${L_P}:${PATH}"
# sbt compile
# cd ${WORKDIR}
# find .ivy2 .sbt -uid 0 -exec chown portage:portage {} \;
# 6. cd ${WORKDIR}
# XZ_OPT=-9 tar --owner=portage --group=portage \
# -cJf /usr/portage/distfiles/${P}-ivy2-deps.tar.xz .ivy2/cache
# XZ_OPT=-9 tar --owner=portage --group=portage \
# -cJf /usr/portage/distfiles/${P}-sbt-deps.tar.xz .sbt
# Uncomment the sbt publishLocal line in src_compile.
# 7. It *might* download more dependencies for src_test, however the presence
# of some of these may cause the src_compile to fail.  So download them
# seperately as root so we can identify the
# additional files.  As root:
# cd ${S}
# ${S}/${P} test
# cd ${WORKDIR}
# XZ_OPT=-9 tar --owner=portage --group=portage \
# -cJf /usr/portage/distfiles/${P}-test-deps.tar.xz \
# $(find .ivy2/cache .sbt -uid 0 -type f -print)
# Note: It might not download anything in src_test, in which case
# ${P}-test-deps.tar.xz is not required.
# 8. Create the binary
# cd $WORDKIR
# XZ_OPT=-9 tar --owner=portage --group=portage \
# -cJf /usr/portage/distfiles/${P}-gentoo-binary.tar.xz ${P} .ivy2/local
# 9. Undo the earlier temporary edits to the ebuild.

src_unpack() {
	# if ! use binary; then
	# 	git-r3_src_unpack
	# fi
	# Unpack tar files only.
	for f in ${A} ; do
		[[ ${f} == *".tgz"* ]] && unpack ${f}
	done
	mv sbt ${P}
}

src_install() {
	dodir /usr/share/sbt
	insinto /usr/share/sbt
	doins -r ${S}/bin
	doins -r ${S}/conf
	doins -r ${S}/lib
	dosym ../usr/share/sbt/conf /etc/sbt
	dosym ../share/sbt/bin/sbt /usr/bin/sbt
	fperms 755 /usr/share/sbt/bin/sbt
	dosym ../share/sbt/bin/java9-rt-export.jar /usr/bin/java9-rt-export.jar
}
